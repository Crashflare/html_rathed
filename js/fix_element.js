/*!
 * fix element v1.2
 * Copyright 2015 Alex Kamuro
 * Licensed under MIT
 */

function elemReset(elem) {
 elem.style.position = "static";
 elem.style.top = null;
}

function elemSet(elem) {
 elem.style.position = "fixed";
 elem.style.top = "10%";
 elem.style.zIndex = "100";
}

function findTop(elem) {
 var top = 0;
 if (!elem.offsetParent) return top;
 do {
     top += elem.offsetTop;
 } while (elem = elem.offsetParent);
 return top;
}

function elemLock(element) {
 var elem = document.querySelector(element);
 elemReset(elem);
 var elem_top = findTop(elem);
 var rect_top = document.body.getBoundingClientRect().top;
 if (Math.abs(rect_top) > elem_top) {
     elemSet(elem);
 }

 window.onscroll = function(){
     var client_width = document.body.clientWidth;
     var scroll_top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
     if ((client_width <= 768) || (scroll_top < elem_top)) {
         elemReset(elem);
         return;
     }
     elemSet(elem);
 }

 window.onresize = function(){
    elemReset(elem);
     elem_top = findTop(elem);
     var client_width = document.body.clientWidth;
     var scroll_top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
     if ((document.body.clientWidth >= 768) && (scroll_top > elem_top)) {
         elemSet(elem);
     }
 }
}


